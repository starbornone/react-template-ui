# React Template with Redux and Tailwind

**Note:** Master branch is currently a WIP move to using TailwindCSS and having a new layout. The older version, using Bulma, is still fully functional and can be found in a branch under the same name.

This is a React web app template for using `react` with `redux` and `redux-thunk`. It comes with Tailwind integrated (although the UI components are a WIP), but there's a Bulma version branch for those who prefer that. It also uses `create-react-app` ([GitHub page](https://github.com/facebook/react)), but feel free to `npm run eject` your way out of it.

Before using this template, have a read of [What Does Redux Do? (and when should you use it?)](https://daveceddia.com/what-does-redux-do/) to determine whether or not you need Redux in your application.

- [React Template with Redux and Tailwind](#react-template-with-redux-and-tailwind)
- [TODO](#todo)
- [Folder Structure](#folder-structure)
- [File & Folder Naming](#file--folder-naming)
  - ["Public" and "Private" folders](#%22public%22-and-%22private%22-folders)
  - [`index.js` versus `{componentName}.js` file names](#indexjs-versus-componentnamejs-file-names)
- [Demo Elements](#demo-elements)
  - [Simple imports](#simple-imports)
  - [Routing with react-router-dom](#routing-with-react-router-dom)
  - [Using React state](#using-react-state)
  - [Using Redux state](#using-redux-state)
  - [Redux Thunk](#redux-thunk)
  - [Using formik](#using-formik)
  - [Using axios](#using-axios)
  - [Handling actions within containers](#handling-actions-within-containers)
- [React & Redux Conventions](#react--redux-conventions)
  - [Functional (Stateless) versus Class (Stateful) Components](#functional-stateless-versus-class-stateful-components)
  - [React State versus Redux State](#react-state-versus-redux-state)
  - [Mapping keys: `index` versus `item.id`](#mapping-keys-index-versus-itemid)
  - [Click events](#click-events)
    - [Handlers](#handlers)
    - [`.bind(this)` versus `() => {}`](#bindthis-versus)
  - [Import specificity](#import-specificity)
- [Package Decisions/Notes](#package-decisionsnotes)
  - [formik? Why not redux-form?](#formik-why-not-redux-form)
  - [There's no need to overuse lodash](#theres-no-need-to-overuse-lodash)
- [Appendix](#appendix)
  - [Included Packages](#included-packages)
    - [Required Packages](#required-packages)
    - [Optional Packages](#optional-packages)
    - [Development Packages](#development-packages)
  - [Other Inclusions](#other-inclusions)
  - [Alternative Packages](#alternative-packages)
  - [Other Package Options](#other-package-options)

# TODO

- Write about having less parameters passing around

# Folder Structure

- **assets**: This will include folders such as "fonts", "images", "styles", and any downloaded packages that don't come via npm.
  - **styles**: `general.css` has some East Asian language styling to start off with.
- **components**: All the reusable components. These should be functional components - no manipulation of the application state within these.
  - **UI**: Basic, more reusable components.
- **containers**: All the "pages". These often contain and manipulate parts of the application state.
- **hoc** (higher order component):
  - **Layout**: All the main layout elements, e.g. the Header and Footer.
- **routes**: All the route files, each containing an array of routes for their own particular section.
- **store**: All the action files. There is a seperate "types" file for containing all action types and an `actions.js` file which is used to export all the actions. The `actions.js` file is for the sake of clarity - an easier overview of all actions that there are - because the individual action files themselves can become quite large and not the best to browse through.
  - **reducers**: All the reducer files. There is an `reducers.js` file which is used to export all the reducers.

# File & Folder Naming

## "Public" and "Private" folders

I'm using "Public" and "Private" folders inside "containers" to separate publicly accessible areas from administration areas. This structure could also be used in "Layout" (under "hoc") and possibly even "components", depending on the complexity and scale of the application.

However, this is only relevant when the difference between the two is significant. For example, a WordPress blog and its admin area would be considered significantly different as the layout and containers within would change drastically. On the other hand, browsing eBay as a guest and as a logged in user merely grants access to some extra containers within the same layout.

## `index.js` versus `{componentName}.js` file names

There's no agreement on how each folder's main file should be named and no objective argument in favour of one over the other. Therefore, in this template, I have decided to go with the latter. This is because it appears to be the safest across all editors in terms of usability. (Whilst VS Code will display the file path in the tab when there are multiple files open with the same name, not all editors do this.)

But it still goes without saying that you are free to change these names to whatever suits you best.

# Demo Elements

## Simple imports

This demo app has been set up so that imports are really simple. For example, instead of this:

```js
import Buttom from '../../../components/Button';
```

We can do this:

```js
import Buttom from 'components/Button';
```

This is because of a few elements. Firstly, we have installed `eslint-plugin-impor`. Then, in the root folder of the client, we set `NODE_PATH=src/` in a `.env` file and added a `jsconfig.json` with the following:

```json
{
  "compilerOptions": {
    "baseUrl": ".",
      "paths": {
      "*": ["src/*"]
    }
  }
}
```

## Routing with react-router-dom

The routes available are determined as arrays of routes in `src/routes/private.js` and `src/routes/public.js`. (See ["Public" and "Private" folders](#%22public%22-and-%22private%22-folders) for why they're split.) Each route requires the following information:

```json
{
  component: Home,
  exact: true,
  path: '/'
}
```

But more details can be housed here. For example, if you want to use the same array of routes to render a menu where each item has an icon, you could add `icon: 'user-alt'` or something similar.

However, for those two main sets of routes to work, they are included in `src/App.js` as:

```jsx
<BrowserRouter>
  <Switch>
    <Route path="/admin" component={Private} />
    <Route path="/" component={Public} />
  </Switch>
</BrowserRouter>
```

Then the arrays of routes are mapped over in `src/containers/Private/Private.js` and `src/containers/Public/Public.js`.

```jsx
{routes.map(route => {
  return <Route
    key={route.id}
    path={route.path}
    exact={route.exact}
    component={route.component}
  />})}
```

For more detailed explanations, see the documentation on the [React Training website](https://reacttraining.com/react-router/web/guides/philosophy).

## Using React state

React state exists within the main component in which it is required. An example of a component using React's state can be found here: `src/containers/Public/Samples/ReactState.js`

## Using Redux state

The main actions files that you will need to be aware of are `src/store/actions.js` and `src/store/actions/types.js`. The former exports all of the actions created in the other files for ease of routing and organisation. The latter contains all of the action types, treated like constants, to help ensure consistency. Organise these however you wish, but I recommend commenting them enough so that it's easy to browse through them.

The main reducers file that you will need to be aware of is `src/store/reducers/reducers.js`. Like the `actions.js`, this is where all of the individual reducers are exported. However, using `combineReducers` here is required to ensure that all of your reducers are included in the application. The `rootReducer` that is exported from this file gets picked up in `src/store/store.js` and `createStore` is applied to it. The `store` that is exported from there gets picked up in `src/index.js` where it's included in the render method via `<Provider store={store}>`.

In order to ensure that everything works from this point on however, all you need to do is make sure that every action you create is exported in `actions.js`, for example:

```jsx
export {
  changeName,
  increaseClicks,
  resetClicks
} from 'store/actions';
```

And every reducer you create is included in `reducers.js`, for example:

```jsx
const appReducer = combineReducers({
  user: userReducer
})
```

Optionally, I would also recommend using the afforementioned `types.js` to keep track of your action types. The format for this usually takes the form of the following:

```js
export const USER_ACTION_TYPE = 'USER_ACTION_TYPE';
```

To see an example using values coming from the Redux state, see `src/containers/Public/Samples/ReduxState.js`. You will see that `connect` is required and that the items coming from state can then be mapped to props like so:

```js
const mapStateToProps = state => ({
  user: state.auth.user
})
```

For more detailed explanations, see the documentation for [Redux](https://redux.js.org/basics), in particular, [[Redux] Usage with React](https://redux.js.org/basics/usage-with-react) and [Immutable Update Patterns](https://redux.js.org/recipes/structuringreducers/immutableupdatepatterns).

[TODO: Add some examples of updating nested objects and arrays in the reducers.]

## Redux Thunk

Redux Thunk middleware allows us to write action creators that return a function instead of an action. The thunk can be used to delay the dispatch of an action or to dispatch only if a certain condition is met. The inner function receives the store methods `dispatch` and `getState` as parameters. For more information, [read this in-depth introduction to thunks in Redux](https://stackoverflow.com/questions/35411423/how-to-dispatch-a-redux-action-with-a-timeout/35415559#35415559).

## Using formik

A formik example can be found in `src/containers/Public/User/Login/Login.js` and `src/containers/Public/User/Register/Register.js`.

## Using axios

Axios examples can be found in `src/store/actions/user.js`.

## Handling actions within containers

The most clear examples of using actions are in `src/containers/Public/Samples/ReactState.js` and `src/containers/Public/Samples/ReduxState.js`. These use handlers and `onClick` events explained in [Click events](#click-events).

However, there are more practical/complex examples using forms (and formik) in `src/containers/Public/User/Login/Login.js` and `src/containers/Public/User/Register/Register.js`.

# React & Redux Conventions

Basically there are three options to have formatting rules in React. It should be quite similar to other ecosystems.

The first approach is to follow a style guide that is embraced by the community. One popular [React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react) was open sourced by Airbnb. Even though you don’t deliberately follow the style guide, it makes sense to read it once to get the basics of formatting in React.

The second approach is to use a linter such as [ESLint](https://eslint.org/). You can integrate it in your tool set when you are at the point of introducing new toolings to your project yourself.

The third and most popular approach is using [Prettier](https://prettier.io/). It is an opinionated code formatter. You can integrate it in your editor or IDE that it formats your code every time you save a file or commit it with git. Perhaps it doesn’t match always your taste, but at least you never need to worry again about code formatting in your own or a team code base.

## Functional (Stateless) versus Class (Stateful) Components

When possible, use functional components. Most parts of the application shouldn't be able to change the application state - they should simply be rendering elements to the DOM.

**Functional (stateless) Component**:

```jsx
const Button = (props) => {
```

**Class (Stateful) Component**:

```jsx
class Public extends Component {
```

## React State versus Redux State

Use React for ephemeral state that doesn't matter to the app globally and doesn't mutate in complex ways. For example, a toggle in some UI element or a form input state. Use Redux for state that matters globally or is mutated in complex ways. For example, cached users or a post draft.

Sometimes you'll want to move from Redux state to React state (when storing something in Redux gets awkward) or the other way around (when more components need to have access to some state that used to be local).

The rule of thumb is: do whatever is less awkward.

## Mapping keys: `index` versus `item.id`

[TODO: Write about how using the index is not recommended when mapping.]

## Click events

### Handlers

It is convention to have a "handler" to handle any action which manipulates the state.

[TODO: Continue explaining this.]

### `.bind(this)` versus `() => {}`

When using `onClick` events, it is recommended to use the `.bind(this)` method as the arrow function, `() => {}`, can be slightly more inefficient and, in rare cases, can lead to some quirky errors. See `ReactState.js` and `ReduxState.js` for examples.

## Import specificity

Whilst this template uses the latter of the two examples below, just pick one style and stick with it.

```jsx
import React from 'react';

class Public extends React.Component {
```

```jsx
import React, { Component } from 'react';

class Public extends Component {
```

# Package Decisions/Notes

## formik? Why not redux-form?

- See [React State versus Redux State](#react-state-versus-redux-state).
- redux-form calls your entire top-level Redux reducer multiple times ON EVERY SINGLE KEYSTROKE. This is fine for small apps, but as your app grows, input latency will continue to increase.
- redux-form is 22.5 kB minified gzipped. formik is 7.8 kB.

For more detailed explanations, see the [formik documentation on GitHub](https://github.com/jaredpalmer/formik).

## There's no need to overuse lodash

Whilst Lodash does come included in this template, please consider whether it is required before using it. It is a great modern JavaScript utility library and widely used by front-end developers. However, when you are targeting modern browsers, you may find out that there are many methods which are already supported natively thanks to ECMAScript5 [ES5] and ECMAScript2015 [ES6]. If you want your project to require fewer dependencies, and you know your target browser clearly, then you may not need Lodash at all.

For more information, see [You-Dont-Need-Lodash-Underscore
 on GitHub](https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore).

# Appendix

## Included Packages

The following packages are included in this template.

### Required Packages

Whilst we can have a standalone React application, there's no need to have a template to do so when such an app is simple enough to set up with just `create-react-app`. So "required" here in this context doesn't mean that this template cannot work without these things. What it means is that, considering the way in which this particular project has been set-up, these packages are an integral part of that.

- **axios** ([GitHub page](https://github.com/axios/axios)): Promise based HTTP client for the browser.
- **react** ([GitHub page](https://github.com/facebook/react)): A declarative, efficient, and flexible JavaScript library for building user interfaces.
- **react-dom** ([GitHub page](https://github.com/facebook/react/tree/master/packages/react-dom)): The entry point to the DOM and server renderers for React.
- **react-redux** ([GitHub page](https://github.com/reduxjs/react-redux)): Official React bindings for Redux.
- **react-router-dom** ([GitHub page](https://github.com/ReactTraining/react-router))
- **react-scripts** ([GitHub page](https://github.com/nearform/react-scripts)): Create React apps with no build configuration.
- **redux** ([GitHub page](https://github.com/reduxjs/redux)): Redux is a predictable state container for JavaScript apps. It helps you write applications that behave consistently, run in different environments (client, server, and native), and are easy to test. On top of that, it provides a great developer experience, such as live code editing combined with a time traveling debugger.
- **redux-thunk** ([GitHub page](https://github.com/reduxjs/redux-thunk)): Thunk middleware for Redux.

### Optional Packages
- **date-fns** ([GitHub page](https://github.com/date-fns/date-fns)): Comprehensive, simple and consistent toolset for manipulating JavaScript dates.
- **formik** ([GitHub page](https://github.com/jaredpalmer/formik)): Formik is a small library that helps you with the 3 most annoying parts: getting values in and out of form state, validation and error messages, and handling form submission.ormik will keep things organized--making testing, refactoring, and reasoning about your forms a breeze.
- **highlight.js** ([GitHub page](https://github.com/highlightjs/highlight.js/)): A syntax highlighter written in JavaScript.
- **lodash** ([GitHub page](https://github.com/lodash/lodash)): Lodash makes JavaScript easier by taking the hassle out of working with arrays, numbers, objects, strings, etc.
- **react-syntax-highlighter** ([GitHub page](https://github.com/conorhastings/react-syntax-highlighter)): A syntax highlighting component for React using the seriously super amazing lowlight and refractor by wooorm.
- **yup** ([GitHub page](https://github.com/jquense/yup)): Yup is a JavaScript object schema validator and object parser. The API and style is heavily inspired by Joi, which is an amazing library but is generally too large and difficult to package for use in a browser.

### Development Packages

- **autoprefixer** ([GitHub page](https://github.com/postcss/autoprefixer)): PostCSS plugin to parse CSS and add vendor prefixes to CSS rules.
- **eslint-plugin-import** ([GitHub page](https://github.com/benmosher/eslint-plugin-import)): Supports linting of ES2015+ (ES6+) import/export syntax and prevents issues with misspelling of file paths and import names.
- **postcss-cli** ([GitHub page](https://github.com/postcss/postcss-cli))
- **redux-immutable-state-invariant** ([GitHub page](https://github.com/leoasis/redux-immutable-state-invariant)): Redux middleware that spits an error on you when you try to mutate your state either inside a dispatch or between dispatches.
- **tailwindcss** ([Offical website](https://tailwindcss.com/)): A highly customizable, low-level CSS framework that gives you all of the building blocks you need to build bespoke designs without any annoying opinionated styles you have to fight to override.

## Other Inclusions

The following have been included as files or CDN links via the public files.

- **FontAwesome 5.10.2**: All SVG-focused JavaScript files are included but only the main `fontawesome.js` and `regular.js` are imported into the app from the start.

## Alternative Packages

- instead of **axios**, use **superagent** ([GitHub page](https://github.com/visionmedia/superagent))
- instead of **enzyme**, use **ava** ([GitHub page](https://github.com/avajs/ava))
- instead of **date-fns**, use **moment** ([GitHub page](https://github.com/moment/moment/))
- instead of **redux-thunk**, use **redux-saga** ([GitHub page](https://github.com/redux-saga/redux-saga))

## Other Package Options

- **chai** ([Github page](https://github.com/chaijs/chai))
- **classnames** ([Github page](https://github.com/JedWatson/classnames))
- **css-modules** ([Github page](https://github.com/css-modules/css-modules))
- **ramda** ([GitHub page](https://github.com/ramda/ramda))
- **mocha** ([Github page](https://github.com/mochajs/mocha))
- **recompose** ([Github page](https://github.com/acdlite/recompose))
- **sinon** ([Github page](https://github.com/sinonjs/sinon))
- **styled-components** ([Github page](https://github.com/styled-components/styled-components))

**Note on mocha and chai usage**: If the whole combo of Mocha (test runner), Chai (assertion), Enzyme (component tests) and Jest (snapshot tests) is too much for you, you can just use Jest with Enzyme. Jest comes with its own assertion API (which was provided by Chai) and own test runner (which was provided by Mocha). That way you can keep your testing environment lightweight.
