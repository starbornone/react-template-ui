/**
 * @name                errorHandler
 * @description         
 * @param   {function}  dispatch
 * @param   {object}    error
 * @param   {string}    type
 * @returns {object}
 */
export const errorHandler = (dispatch, error, type) => {
  if (error.response) {
    dispatch({ type, payload: error.response.data })
  } else if (type) {
    dispatch({ type, payload: { message: error } })
  }
}

/**
 * @name                updateObject
 * @description         
 * @param   {object}    oldObject
 * @param   {object}    updatedProperties
 * @returns {object}
 */
export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  }
}

/**
 * @name                insertArrayItem
 * @description         
 * @param   {array}     array
 * @param   {object}    action
 * @returns {array}
 */
export const insertArrayItem = (array, action) => {
  let newArray = array.slice()
  newArray.splice(action.index, 0, action.item)
  return newArray
}

/**
 * @name                removeArrayItem
 * @description         
 * @param   {array}     array
 * @param   {object}    action
 * @returns {array}
 */
export const removeArrayItem = (array, action) => {
  let newArray = array.slice()
  newArray.splice(action.index, 1)
  return newArray
}

/**
 * @name                updateArrayObject
 * @description         
 * @param   {array}     sourceArray
 * @param   {array}     newArray
 * @returns {array}
 */
export const updateArrayObject = (array, action) => {
  return array.map((item, index) => {
    if (index !== action.index) {
      return item
    }
    return {
      ...item,
      ...action.item
    }
  })
}