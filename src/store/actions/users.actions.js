import axios from 'store/actions/axios-config';
import { errorHandler } from 'store/helpers';

export const USERS_CLICKS_INCREASE = '[USERS] CLICKS INCREASE';
export const USERS_CLICKS_RESET = '[USERS] CLICKS RESET';
export const USERS_DELETE_START = '[USERS] DELETE START';
export const USERS_DELETE_COMPLETE = '[USERS] DELETE COMPLETE';
export const USERS_GET_START = '[USERS] GET START';
export const USERS_GET_COMPLETE = '[USERS] GET COMPLETE';
export const USERS_LIST_START = '[USERS] LIST START';
export const USERS_LIST_COMPLETE = '[USERS] LIST COMPLETE';
export const USERS_NAME_CHANGE = '[USERS] NAME CHANGE';

/**
 * User actions
 */

/**
 * @name                deleteUser
 * @description         Delete a single user
 * @param   {integer}   userId
*/
export function deleteUser(userId) {
  return function (dispatch) {
    dispatch({ type: USERS_DELETE_START })
    axios.delete('/users/' + userId)
      .then(response => dispatch({ type: USERS_DELETE_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error.response.data))
  }
}

/**
 * @name                getUser
 * @description         Get the details for a single user by their id
 * @param   {integer}   userId
*/
export function getUser(userId) {
  return function (dispatch) {
    dispatch({ type: USERS_GET_START })
    axios.get('/users/' + userId)
      .then(response => dispatch({ type: USERS_GET_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error.response.data))
  }
}

/**
 * @name                getUserList
 * @description         Get the details for all users
*/
export function getUserList() {
  return function (dispatch) {
    dispatch({ type: USERS_LIST_START });
    axios.get('/users/list')
      .then(response => dispatch({ type: USERS_LIST_COMPLETE, payload: response.data }))
      .catch(error => errorHandler(dispatch, error.response.data))
  }
}

/**
 * React versus Redux example actions
 */

export function changeName(newName) {
  return function (dispatch) {
    dispatch({ type: USERS_NAME_CHANGE, payload: newName })
  }
}

export function decreaseClicks(oldClicks) {
  return function (dispatch) {
    dispatch({ type: USERS_CLICKS_INCREASE, payload: oldClicks - 1 })
  }
}

export function increaseClicks(oldClicks) {
  return function (dispatch) {
    dispatch({ type: USERS_CLICKS_INCREASE, payload: oldClicks + 1 })
  }
}

export function resetClicks() {
  return function (dispatch) {
    dispatch({ type: USERS_CLICKS_RESET })
  }
}