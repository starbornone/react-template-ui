import * as Actions from '../actions';
import { updateObject } from 'store/helpers';

const INITIAL_STATE = {
  clicks: 0,
  data: {},
  list: [],
  loading: false,
  name: 'Bob'
}

const userGetComplete = (state, action) => {
  return updateObject(state, { details: action.payload, loading: false });
}

const userListComplete = (state, action) => {
  return updateObject(state, { loading: false, list: action.payload });
}

/** Sample actions */
const userClicksIncrease = (state, action) => {
  return updateObject(state, { clicks: action.payload });
}

const userClicksReset = (state) => {
  return updateObject(state, { clicks: 0 });
}

const userNameChange = (state, action) => {
  return updateObject(state, { name: action.payload });
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case Actions.USERS_GET_COMPLETE: return userGetComplete(state, action);
    case Actions.USERS_LIST_COMPLETE: return userListComplete(state, action);
    /* Sample actions */
    case Actions.USERS_NAME_CHANGE: return userNameChange(state, action);
    case Actions.USERS_CLICKS_INCREASE: return userClicksIncrease(state, action);
    case Actions.USERS_CLICKS_RESET: return userClicksReset(state);
    default: return state;
  }
}