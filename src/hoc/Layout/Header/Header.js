import React from 'react';
import Navbar from 'hoc/Layout/Navbar/Navbar';

const Header = ({ user, isAuthenticated }) => {
  return <header>
    <nav id="header" className="fixed w-full z-30 top-0">
      <Navbar isAuthenticated={isAuthenticated} user={user} />
      <hr className="border-b border-dark-300 opacity-25 my-0 py-0" />
    </nav>
  </header>
}

export default Header;
