  /**
   * Here we are just faking some data coming back from our database. Since it's a little specific, we aren't building
   * this feature into the template.
   * The menus are set up in this manner for two reasons:
   * 1. We can use a single component to render each individual menu item by mapping over the array of items.
   * 2. This allows them to be created, modified, or deleted by a user. The menu data can be saved in the database and then loaded into the client.
   */
  export const menus = [
  {
    id: 'menu-01',
    items: [
      {
        id: 0,
        name: 'Public Page',
        path: '/'
      }, {
        id: 1,
        name: 'Private Page',
        path: '/admin'
      }
    ],
  }, {
    id: 'menu-02',
    items: [
      {
        icon: 'comments',
        id: 0,
        name: 'Dummy Link',
        path: 'menu-item-01'
      }, {
        children: [
          {
            id: 0,
            name: 'Menu Item 01',
            path: 'menu-item-01'
          }, {
            id: 1,
            name: 'Menu Item 02',
            path: 'menu-item-02'
          }
        ],
        icon: 'dice',
        id: 1,
        name: 'Dummy Menu 01',
        path: 'menu-item-00'
      }, {
        children: [
          {
            id: 0,
            name: 'Menu Item 01',
            path: 'menu-item-01'
          }, {
            id: 1,
            name: 'Menu Item 02',
            path: 'menu-item-02'
          }
        ],
        icon: 'flask',
        id: 2,
        name: 'Dummy Menu 02',
        path: 'menu-item-02'
      }
    ],
    multiple: false,
    name: 'Test Menu'
  }, {
    id: 'menu-03',
    items: [
      {
        children: [
          {
            id: 0,
            name: 'Menu Item 01',
            path: 'menu-item-01'
          }, {
            id: 1,
            name: 'Menu Item 02',
            path: 'menu-item-02'
          }
        ],
        icon: 'users',
        id: 0,
        name: 'Dummy Menu 03',
        path: 'menu-item-00'
      }, {
        children: [
          {
            id: 0,
            name: 'Menu Item 01',
            path: 'menu-item-01'
          }, {
            id: 1,
            name: 'Menu Item 02',
            path: 'menu-item-02'
          }
        ],
        icon: 'desktop',
        id: 1,
        name: 'Dummy Menu 04',
        path: 'menu-item-01'
      }, {
        icon: 'comments',
        id: 2,
        name: 'Dummy Link',
        path: 'menu-item-01'
      }
    ],
    multiple: true,
    name: 'Test Menu'
  }
];