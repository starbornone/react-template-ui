import Home from 'containers/Public/Home/Home';

/**
 * User containers
 */
import UserLogin from 'containers/Public/User/Login/Login';
import UserLogout from 'containers/Public/User/Logout/Logout';
import UserRegister from 'containers/Public/User/Register/Register';

export const routes = [
  {
    component: Home,
    exact: true,
    id: 'public-home-01',
    path: '/'
  },
  /**
   * User routes
   */
  {
    component: UserLogin,
    exact: true,
    id: 'public-user-login-01',
    path: '/login'
  }, {
    component: UserLogout,
    exact: true,
    id: 'public-user-logout-01',
    path: '/logout'
  }, {
    component: UserRegister,
    exact: true,
    id: 'public-user-register-01',
    path: '/register'
  }
]