import { useTimeout } from '@fuse/hooks';
import LinearProgress from '@material-ui/core/LinearProgress';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

function Loading(props) {
  const [showLoading, setShowLoading] = useState(!props.delay);

  useTimeout(() => {
    setShowLoading(true);
  }, props.delay);

  if (!showLoading) {
    return null;
  }

  return (
    <div className="flex flex-1 flex-col items-center justify-center">
      <div className="text-20 mb-16" color="textSecondary">
        Loading...
			</div>
      <LinearProgress className="w-xs" color="secondary" />
    </div>
  );
}

Loading.propTypes = {
  delay: PropTypes.oneOfType([PropTypes.number, PropTypes.bool])
};

Loading.defaultProps = {
  delay: false
};

export default Loading;
