import React, { memo } from 'react';
import { withFormsy } from 'formsy-react';

function Select(props) {
  function changeValue(event) {
    props.setValue(event.target.value);
    if (props.onChange) {
      props.onChange(event);
    }
  }

  const { errorMessage, config: { label, typeOptions }, value } = props;

  return (
    <div className="p-1">
      <label className="capitalize">{label}</label>
      <select className="bg-dark-700 p-2 w-full" onChange={changeValue} value={value}>
        {typeOptions.options && typeOptions.options.map(option => <option key={option.value} value={option.value}>{option.display}</option>)}
      </select>
      <span>{errorMessage}</span>
    </div>
  )
}

export default memo(withFormsy(Select));