import React, { Component } from 'react';
import { connect } from 'react-redux';

import { logoutUser } from 'auth/store/actions';

import Message from 'components/Message/Message';

class Logout extends Component {
  componentDidMount() {
    const { logoutUser } = this.props;
    logoutUser();
  }

  render() {
    return <Message header="You have been logged out." type="success">
      You have been logged out.
    </Message>
  }
}

export default connect(null, { logoutUser })(Logout);