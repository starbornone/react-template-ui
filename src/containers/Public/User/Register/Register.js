import _ from '@lodash';
import React, { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Formsy from 'formsy-react';

import { submitRegister } from 'auth/store/actions';

import Input from 'components/Input';

function Register(props) {
  const dispatch = useDispatch();

  const [isFormValid, setIsFormValid] = useState(false);
  const formRef = useRef(null);

  function disableButton() {
    setIsFormValid(false);
  }

  function enableButton() {
    setIsFormValid(true);
  }

  function handleSubmit(model) {
    dispatch(submitRegister(model));
  }

  return <section className="bg-dark-500 py-8">
    <div className="container max-w-5xl mx-auto m-8">

      <h2>Register</h2>

      <div className="content">
        <p>Fill out the form below to create an account.</p>
      </div>

      <Formsy
        onValidSubmit={handleSubmit}
        onValid={enableButton}
        onInvalid={disableButton}
        ref={formRef}
        className="flex flex-col justify-center w-full"
      >
        <Input
          label="Email"
          name="email"
          type="email"
        />

        <div className="field is-horizontal">
          <div className="field-label is-normal">
            <label className="label">Password</label>
          </div>
          <div className="field-body">
            <Input
              label="Password"
              name="password"
              type="password"
            />
            <Input
              label="Repeat Password"
              name="passwordRepeat"
              type="password"
            />
          </div>
        </div>

        <Input
          label="Name"
          name="name"
          type="text"
        />

        <div className="field is-grouped is-grouped-right">
          <div className="control">
            <Button
              className="normal-case"
              color="default"
              disabled={!isFormValid}
              type="submit"
              variant="contained"
            >
              Submit
          </Button>
          </div>
          <div className="control">
            <Button type="reset">
              Reset
          </Button>
          </div>
        </div>

      </Formsy>

    </div>
  </section>
}

export default Register;