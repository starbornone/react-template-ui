import React from 'react';

import ReactState from 'containers/Public/Samples/ReactState';
import ReduxState from 'containers/Public/Samples/ReduxState';

const Home = () => {
  return <section className="bg-dark-500 py-8">
    <div className="container max-w-5xl mx-auto m-8">
      
      <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center">Examples</h1>

      <ReactState />
      <ReduxState />

    </div>
  </section>
}



export default Home;