import React, { useRef, useState } from 'react';

import Button from '@material-ui/core/Button';
import Formsy from 'formsy-react';

import Input from 'components/Input';

function ReactState(props) {
  const [clicks, setClicks] = useState(0);
  const [name, setName] = useState('Bob');

  const formRef = useRef(null);

  function increaseClickHandler() {
    setClicks(clicks + 1);
  }

  function decreaseClickHandler() {
    setClicks(clicks - 1);
  }

  function nameChangeHandler(value) {
    setName(value);
  }

  function resetClickHandler() {
    setClicks(0);
  }

  return <div className="flex flex-wrap lg:items-end">
    <div className="w-5/6 sm:w-1/2 p-6">

      <h3 className="text-3xl font-bold leading-none">React State Samples</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula.</p>

    </div>
    <div className="w-full sm:w-1/2 p-6">

      <Formsy
        ref={formRef}
        className="flex flex-col justify-center w-full"
      >
        <Input
          name="name"
          onChange={e => nameChangeHandler(e.target.value)}
          type="text"
          value={name}
        />
        <div className="mt-2 mb-4">
          <strong>Name:</strong> {name}
        </div>
        <div className="flex flex-wrap items-center justify-between">
          <Button
            className="normal-case"
            color="primary"
            onClick={increaseClickHandler}
            type="button"
            variant="outlined"
          >
            Increase Clicks
          </Button>
          <Button
            className="normal-case"
            color="default"
            onClick={decreaseClickHandler}
            type="button"
            variant="outlined"
          >
            Decrease Clicks
          </Button>
          <Button
            className="normal-case"
            color="secondary"
            onClick={resetClickHandler}
            type="button"
            variant="outlined"
          >
            Reset Clicks
          </Button>
        </div>
        <div className="mt-2">
          <strong>Clicks:</strong> {clicks}
        </div>
      </Formsy>

    </div>
  </div>
}

export default ReactState;