import React, { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Button from '@material-ui/core/Button';
import Formsy from 'formsy-react';

import { changeName, decreaseClicks, increaseClicks, resetClicks } from 'store/actions';

import Input from 'components/Input';

function ReduxState(props) {
  const dispatch = useDispatch();
  const users = useSelector(({ users }) => users);

  const formRef = useRef(null);

  function increaseClickHandler() {
    dispatch(increaseClicks(users.clicks));
  }

  function decreaseClickHandler() {
    dispatch(decreaseClicks(users.clicks));
  }

  function nameChangeHandler(value) {
    dispatch(changeName(value));
  }

  function resetClickHandler() {
    dispatch(resetClicks());
  }

  return <div className="flex flex-wrap flex-col-reverse sm:flex-row lg:items-end">
    <div className="w-full sm:w-1/2 p-6 mt-6">

      <Formsy
        ref={formRef}
        className="flex flex-col justify-center w-full"
      >
        <Input
          name="name"
          onChange={e => nameChangeHandler(e.target.value)}
          type="text"
          value={users.name}
        />
        <div className="mt-2 mb-4">
          <strong>Name:</strong> {users.name}
        </div>
        <div className="flex flex-wrap items-center justify-between">
          <Button
            className="normal-case"
            color="primary"
            onClick={increaseClickHandler}
            type="button"
            variant="outlined"
          >
            Increase Clicks
          </Button>
          <Button
            className="normal-case"
            color="default"
            onClick={decreaseClickHandler}
            type="button"
            variant="outlined"
          >
            Decrease Clicks
          </Button>
          <Button
            className="normal-case"
            color="secondary"
            onClick={resetClickHandler}
            type="button"
            variant="outlined"
          >
            Reset Clicks
          </Button>
        </div>
        <div className="mt-2">
          <strong>Clicks:</strong> {users.clicks}
        </div>
      </Formsy>

    </div>
    <div className="w-full sm:w-1/2 p-6 mt-6">
      <div className="align-middle">

        <h3 className="text-3xl font-bold leading-none">Redux State Samples</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at ipsum eu nunc commodo posuere et sit amet ligula.</p>

      </div>
    </div>
  </div>
}

export default ReduxState;