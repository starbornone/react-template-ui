import React from 'react';

const Home = () => {
  return <div>
    <h2>Admin Area</h2>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque sed suscipit purus, tincidunt varius lectus. Sed ultricies tempus est et vulputate. Donec interdum, nisi id rhoncus dictum, metus dui malesuada odio, quis sollicitudin ante leo malesuada urna. Aenean euismod aliquam risus, commodo commodo lectus blandit quis. Sed viverra a ipsum at commodo. Ut sit amet elit id orci interdum sollicitudin. Vestibulum eget risus varius arcu dictum molestie. </p>
  </div>
}

export default Home;