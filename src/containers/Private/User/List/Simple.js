import React from 'react';

import Card from 'components/Card';

const UserSimple = ({ details: { email, id, name } }) => {
  return <Card footer={[
    { className: 'has-text-danger', to: '/admin/users/' + id + '/delete', text: 'Delete User', type: 'LINK' },
    { to: '/admin/users/' + id + '/edit', text: 'Edit User', type: 'LINK' },
    { to: '/admin/users/' + id, text: 'More Details', type: 'LINK' }
  ]} title={name}>
    <strong>Email:</strong> {email}
  </Card>
}

export default UserSimple;