import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUserList } from 'store/actions';

import UserSimple from './Simple';

class List extends Component {
  componentDidMount() {
    const { getUserList } = this.props;
    getUserList();
  }

  render() {
    const { user: { list } } = this.props;

    return <div>
      {list.map(user => <UserSimple details={user} key={user.id} />)}
    </div>
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
})

export default connect(mapStateToProps, { getUserList })(List);
