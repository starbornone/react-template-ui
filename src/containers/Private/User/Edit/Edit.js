import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getUser } from 'store/actions';

class UserEdit extends Component {
  componentDidMount() {
    const { getUser, match: { params: { userId } } } = this.props;
    getUser(userId);
  }

  render() {
    const { user: { details } } = this.props;

    return <div>
      <h1 className="title">{details.name}</h1>
      <h2 className="subtitle">{details.email}</h2>
    </div>
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
})

export default connect(mapStateToProps, { getUser })(UserEdit);